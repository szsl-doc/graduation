# 资源获取： 基于JSP的毕业设计选题系统的设计与实现

## 微信扫码
![qrcode](https://gitlab.com/szsl-doc/graduation/-/raw/main/images/selectSystem/ktt-selectSystem.png)




----
🏵️ 正确运行该项目的前提是，安装、配置符合要求的开发工具以及兼容的开发环境，这一点需注意。

🏵️ **开发工具**：
- 应用服务器: TOMCAT 8.5.98
- 数据库: MySQL 8.0.30
- JAVA : JDK 1.8.0_77

- IDEA : IntelliJ IDEA 2021.2.2 (Community Edition)
- Maven : 3.6.3
- MySQL Workbench : 8.0.31



